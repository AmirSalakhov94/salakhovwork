package com.adhive.parser.file;

import com.adhive.parser.model.User;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class WriteFile {
    private static String FILE_NAME = "./src/main/resources/UsersData.xlsx";

    public static void writeExlFile(List<User> users) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("UsersData");
        int rowNum = 0;
        for (User user : users) {
            Row row = sheet.createRow(rowNum++);
            Cell cell = row.createCell(0);
            cell.setCellValue((int)user.getVkId());
            cell = row.createCell(1);
            cell.setCellValue(user.getName());
            cell = row.createCell(2);
            cell.setCellValue(user.getCountry());
            cell = row.createCell(3);
            cell.setCellValue(user.getCity());
            cell = row.createCell(4);
            cell.setCellValue(user.getAge());
            cell = row.createCell(5);
            cell.setCellValue(user.getSex());
        }

        try {
            File file = new File(FILE_NAME);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            if (workbook != null) {
                workbook.write(fileOutputStream);
                workbook.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
