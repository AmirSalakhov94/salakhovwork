package com.adhive.parser.service;

import com.adhive.parser.file.WriteFile;
import com.adhive.parser.model.User;
import com.adhive.parser.properties.VKProperties;
import com.adhive.parser.singleton.VKUserAuth;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.users.responses.SearchResponse;
import com.vk.api.sdk.queries.users.UserField;
import com.vk.api.sdk.queries.users.UsersSearchSex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.social.vkontakte.connect.VKontakteConnectionFactory;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class VKService {
    @Autowired
    private VKProperties vkProperties;

    private UserAuthResponse userAuthResponse;
    private UserActor userActor;
    private TransportClient transportClient = HttpTransportClient.getInstance();
    private VkApiClient vk = new VkApiClient(transportClient);

    public String getAuthorizationURL() {
        VKontakteConnectionFactory vKontakteConnectionFactory = new VKontakteConnectionFactory(vkProperties.getAppId(), vkProperties.getAppSecret());
        OAuth2Operations oAuth2Operations = vKontakteConnectionFactory.getOAuthOperations();
        OAuth2Parameters oAuth2Parameters = new OAuth2Parameters();
        oAuth2Parameters.setRedirectUri(vkProperties.getRedirectUri());
        oAuth2Parameters.setScope("offline");
        return oAuth2Operations.buildAuthorizeUrl(oAuth2Parameters);
    }

    public void initAuthorization(String code) {
        userAuthResponse = VKUserAuth.getInstance(vk, vkProperties, code);
        userActor = new UserActor(userAuthResponse.getUserId(), userAuthResponse.getAccessToken());
    }

    private List<User> searchUsers() {
        List<User> users = new ArrayList();
        if (userAuthResponse != null && userActor != null) {
            try {
                SearchResponse searchResponse = (SearchResponse)vk.users()
                        .search(userActor)
                        .count(1000)
                        .fields(UserField.COUNTRY, UserField.CITY, UserField.SEX, UserField.BDATE)
                        .country(1)
                        .city(18)
                        .ageFrom(25)
                        .ageTo(34)
                        .sex(UsersSearchSex.MALE)
                        .execute();
                searchResponse.getItems().forEach((item) -> {
                    String _country = item.getCountry() != null ? item.getCountry().getTitle() : null;
                    String _city = item.getCity() != null ? item.getCity().getTitle() : null;
                    users.add(new User(item.getId(), item.getFirstName(), _country, _city, getAgeByBDate(item.getBdate()), "Мужской"));
                });
            } catch (ApiException var9) {
                var9.printStackTrace();
            } catch (ClientException var10) {
                var10.printStackTrace();
            }
        }

        return users;
    }

    public void writeFile() {
        WriteFile.writeExlFile(searchUsers());
    }

    private String getAgeByBDate(String bDate) {
        String age = "25-34";
        if (bDate == null)
            return age;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.mm.yyyy");
        Date date = null;
        try {
            date = simpleDateFormat.parse(bDate);
        } catch (ParseException e) {
            return age;
        }
        Date currentDate = new Date();
        Integer year = (currentDate.getYear() - date.getYear());
        return year.toString();
    }
}
