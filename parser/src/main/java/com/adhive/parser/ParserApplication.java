package com.adhive.parser;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.net.URI;

@SpringBootApplication
public class ParserApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(ParserApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response
				= restTemplate.exchange(new URI("http://127.0.0.1:8080/createVKAuthorization"), HttpMethod.GET,null, String.class);
	}
}
