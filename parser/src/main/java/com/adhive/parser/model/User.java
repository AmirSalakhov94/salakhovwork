package com.adhive.parser.model;

public class User {
    private Integer vkId;
    private String name;
    private String country;
    private String city;
    private String age;
    private String sex;

    public User(Integer vkId, String name, String country, String city, String age, String sex) {
        this.vkId = vkId;
        this.name = name;
        this.country = country;
        this.city = city;
        this.age = age;
        this.sex = sex;
    }

    public Integer getVkId() {
        return this.vkId;
    }

    public void setVkId(Integer vkId) {
        this.vkId = vkId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAge() {
        return this.age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
