package com.adhive.parser.singleton;

import com.adhive.parser.properties.VKProperties;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.UserAuthResponse;

public class VKUserAuth {
    private static UserAuthResponse userAuthResponse;

    public static UserAuthResponse getInstance(VkApiClient vk, VKProperties vkProperties, String code) {
        if (userAuthResponse == null) {
            try {
                userAuthResponse = (UserAuthResponse)vk.oauth().
                        userAuthorizationCodeFlow(Integer.parseInt(vkProperties.getAppId()), vkProperties.getAppSecret(),
                                vkProperties.getRedirectUri(), code).execute();
            } catch (ApiException var4) {
                var4.printStackTrace();
            } catch (ClientException var5) {
                var5.printStackTrace();
            }
        }

        return userAuthResponse;
    }
}
