package com.adhive.parser.controller;

import com.adhive.parser.service.VKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/")
public class VKController {
    @Autowired
    VKService vkService;

    @GetMapping("/createVKAuthorization")
    public void createVKAuthorization() {
        String authorizationURL = vkService.getAuthorizationURL();

        try {
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("rundll32 url.dll, FileProtocolHandler " + authorizationURL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/vk")
    public void createVKAccessToken(@RequestParam("code") String code) {
        vkService.initAuthorization(code);
        vkService.writeFile();
        System.exit(0);
    }
}
